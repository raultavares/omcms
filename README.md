### OmCMS - Open Minds - Content Management System - www.openminds.zone
  
#### Managing the project:
#### We use `make` to make our life easie.
- Start the project: `make build`  
- `make <enter>` display all available options  
- To run Django project `manage` commands in the container:   
    Run `docker-compose run backend python project/manage.py [command]'`
- In `dev` mode - defined in `.env` file - WSGI server is Django dev server, so we cam have reload on change.
- In `prod` mode - defined in `.env` file - the WSGI server is GUnicorn. It should have a webserver like NGinX on front of it.
 
#### Using the Open Minds Content Management System (OmCMS):

Content management is based around two basic concepts: 'category' and 'content articles'.  
- 'Category' defines a menu entry. Selecting that entry renders all 'articles' with same category.  
- 'Category' have the following fields:  
    - 'category' name - to display on url when rendering the area.  
    - 'text show' - text to display on menu and as header of the area when rendering.  
    - 'show on menu' - if unset, category will not be part of the menu.  
    - 'order' - order of appearance in the menu.  
    - 'active' - if unset category will not be rendered.  
- 'Content' articles have the following fields:   
    - 'category'   
    - 'title'   
    - 'subject'   
    - 'card body' - text to display as a teaser when the article is being shown in home page.   
    - 'content' - body text   
    - 'teaser image' - image to be displayed on the top of the card when listing the content.   
    - 'content image'   
    - 'order'   
    - 'show on home page'   
    - 'read more'   
    - 'publish date' - if set content will not be displayed before the date indicated.   
    - 'active' - if unset content will be not rendered.   
    This content fields are displayed in the different templates, creating different page layouts.   
- Special content:   
    - 'header' - holds the image to the header   
    - 'footer' - holds the image to the footer   

#### TODO:
- Create a 'awaiting approval' state 
- Create a 'Site configuration page on Admin'

- Add https to Nginx
- Automate the DB creation for non-standard PostGres setup
- Decouple frontend
- Setup own NGinx server
- Setup supervisor
- Make media content upload storage by article.
 

